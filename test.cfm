<cfquery name="getDogumYeri" datasource="students">
    SELECT DOGUM_YERI_ID,DOGUM_YERI
    FROM Dogum_Yeri
</cfquery>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<input type="text"><br>
<select name="DOGUM_YERI_ID" id="DOGUM_YERI">
    <option disabled="disabled" selected="selected">Choose option</option>
    <cfoutput query="getDogumYeri">
        <option value="#DOGUM_YERI_ID#">#DOGUM_YERI#</option>
    </cfoutput>
</select>

<script>
    //jQuery extension method:
    jQuery.fn.filterByText = function(textbox) {
        return this.each(function() {
            var select = this;
            var options = [];
            $(select).find('option').each(function() {
                options.push({
                    value: $(this).val(),
                    text: $(this).text()
                });
            });
            $(select).data('options', options);

            $(textbox).bind('change keyup', function() {
                var options = $(select).empty().data('options');
                var search = $.trim($(this).val());
                var regex = new RegExp(search, "gi");

                $.each(options, function(i) {
                    var option = options[i];
                    if (option.text.match(regex) !== null) {
                        $(select).append(
                            $('<option>').text(option.text).val(option.value)
                        );
                    }
                });
            });
        });
    };

    // You could use it like this:

    $(function() {
        $('select').filterByText($('input'));
    });
</script>